import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})

socket.connect()

let channel = socket.channel("messages:public", {})

let messages = document.getElementById("messages_list")

channel.on("new_message", content => {
  let message = content.body
  appendMessage(message)
})

function appendMessage(content) {
  let messageItem = document.createElement("li")
  messageItem.innerText = "> " + content
  messages.appendChild(messageItem)
}

channel.join()
  .receive("ok", resp => {
    let history = resp.history
    console.log("Joined successfully", history)
    history.forEach((e) =>
      appendMessage(e)
    )
  })
  .receive("error", resp => { console.log("Unable to join", resp) })

let button = document.getElementById("btn_send")
let inpoot = document.getElementById("user_inpoot")

button.addEventListener("click", _ => {
  let message = inpoot.value
  send(message)
  inpoot.value = ""
}, false)

function send(message) {
    channel.push("new_message", {body: message})
}

export default socket

