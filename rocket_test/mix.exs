defmodule RocketTest.MixProject do
  use Mix.Project

  def project do
    [
      app: :rocket_test,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {RocketTest, []},
      applications: [:nadia, :amqp],
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:amqp, "~> 1.0"},
      {:ranch_proxy_protocol, github: "heroku/ranch_proxy_protocol", override: true},
      {:nadia, "~> 0.4.4"}
    ]
  end
end
