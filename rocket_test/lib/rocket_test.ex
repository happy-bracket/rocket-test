defmodule RocketTest do
  use Application

  def start(_, _) do
    children = [
      %{id: Telegram.Bot, start: {Telegram.Bot, :start_link, [[]]}},
      %{id: Rabbit.Producer, start: {Rabbit.Producer, :start_link, [[]]}}
    ]
    opts = [strategy: :one_for_one, name: Rocketnode2.Supervisor]
    Supervisor.start_link(children, opts)
  end

end
