defmodule Telegram.Data do
    import Nadia.Model

    def extract_message_list(updates) do
        updates
        |> Enum.map(&(extract_message(&1)))
        |> Enum.filter(fn {text, _} -> text != :nil end)
        |> Enum.sort(fn {_, ts1}, {_, ts2} -> (ts1 < ts2) end)
        |> Enum.map(fn {content, _} -> content end)
    end

    defp extract_message(%{message: %{text: text, date: timestamp}}), do: {text, timestamp}

end